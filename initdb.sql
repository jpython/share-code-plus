DROP TABLE IF EXISTS snippets;
DROP TABLE IF EXISTS users;

CREATE TABLE snippets(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	              urlid VARCHAR(15) UNIQUE,
		      lang VARCHAR(20),
		      code TEXT
	     );

CREATE TABLE users(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	           snippet INTEGER NOT NULL,
	           ip VARCHAR(40),
		   agent VARCHAR(100),
		   dt DATETIME,
		   FOREIGN KEY(snippet) REFERENCES snippets(id)
	     );
